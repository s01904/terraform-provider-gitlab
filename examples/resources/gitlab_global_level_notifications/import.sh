# Note: You can import a global notification state using "gitlab" as the ID.
# The ID will always be gitlab, because the global notificatio only exists
# once per user

terraform import gitlab_global_level_notifications.example gitlab
